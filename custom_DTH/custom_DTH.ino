#include <DHT22.h>
// Only used for sprintf
#include <stdio.h>

#define DHT22_PIN 11
#define PAN_PIN 9

#define INTERVAL_MS 300 // maximum 999


DHT22 myDHT22(DHT22_PIN);

int len = 10;

String FALSE = "false";
String dht = "";
String pm = "";
boolean state = false;


void setup(void)
{
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(PAN_PIN, OUTPUT);
}

void loop(void)
{ 
  String dhtTmp = dht22();
  String pmTmp = getPM();
  if (dhtTmp != FALSE) { 
    dht = dhtTmp; 
  }
  if (pmTmp != FALSE) { 
    pm = pmTmp; 
  }

  if (Serial.available() > 0) {
    int power = Serial.read();
    if (power == 49) { // 1
      state = ctrlPower(true);
    } else if (power == 48) { // 0
      state = ctrlPower(false);
    }
  }

  Serial.print(dht + "," + pm + "," + state); // temperature, humidity, pm2_5, pm10, power
  delay(INTERVAL_MS);
}

String dht22() 
{
  String result = "false";
  DHT22_ERROR_t errorCode;
  errorCode = myDHT22.readData();
  switch(errorCode)
  {
  case DHT_ERROR_NONE:
    float temperature = (float)myDHT22.getTemperatureCInt();
    float humidity = (float)myDHT22.getHumidityInt();
    result = String(temperature/10, 1) + "," + String(humidity/10, 1);
    break;
    /*case DHT_ERROR_CHECKSUM:
     Serial.print("check sum error ");
     Serial.print(myDHT22.getTemperatureC());
     Serial.print("C ");
     Serial.print(myDHT22.getHumidity());
     //Serial.println("%");
     break;
     case DHT_BUS_HUNG:
     Serial.println("BUS Hung ");
     break;
     case DHT_ERROR_NOT_PRESENT:
     Serial.println("Not Present ");
     break;
     case DHT_ERROR_ACK_TOO_LONG:
     Serial.println("ACK time out ");
     break;
     case DHT_ERROR_SYNC_TIMEOUT:
     Serial.println("Sync Timeout ");
     break;
     case DHT_ERROR_DATA_TIMEOUT:
     Serial.println("Data Timeout ");
     break;
     case DHT_ERROR_TOOQUICK:
     Serial.println("Polled to quick ");
     break;*/
  }
  return result;
}

String getPM() {
  // read serial data
  String result = "";
  byte buf[10];
  Serial1.readBytes((char*)buf, len);
  //Serial.print("I received: ");
  //for (int i = 0; i < len; i++) {
  //  Serial.println(buf[i], DEC);
  //}

  // checksum
  byte checksum = 0;
  for (int i = 2; i < 8; i++) {
    checksum += buf[i];
  }
  //Serial.print("checksum: ");
  //Serial.println(checksum, DEC);

  // check
  //Serial.println("check: ");
  if (buf[0] == 170 && buf[9] == 171 && buf[8] == checksum) {
    float pm2_5 = float((buf[3] * 256) + buf[2]) / 10;
    float pm10 = float((buf[5] * 256) + buf[4]) / 10;
    //Serial.println(String(pm2_5, 1) + "," + String(pm10, 1));
    result = String(pm2_5, 1) + "," + String(pm10, 1);
  } 
  else {
    result = "false";
    //Serial.println("false");
  }
  return result;
}

boolean ctrlPower(boolean ctrl) {
  if (ctrl) {
    analogWrite(PAN_PIN, 255); // minimum 165
  } 
  else {
    analogWrite(PAN_PIN, 0);
  }
  return ctrl;
}


